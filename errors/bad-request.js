//* 400 Bad Request: The server cannot understand the request due to invalid syntax

import { StatusCodes } from "http-status-codes";
class BadRequestError extends Error {
  constructor(message) {
    super(message);
    this.statusCode = StatusCodes.BAD_REQUEST;
  }
}

export { BadRequestError };
