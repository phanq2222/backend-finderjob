//* 401 Unauthorized: The request requires authentication or the user does not have valid credentials
import { StatusCodes } from "http-status-codes";
class UnauthorizedError extends Error {
  constructor(message) {
    super(message);
    this.statusCode = StatusCodes.UNAUTHORIZED;
  }
}

export { UnauthorizedError };
