import dotenv from "dotenv";

dotenv.config();

const nodemailerConfig = {
  service: "Gmail",
  port: 465,
  secure: true,
  auth: {
    user: process.env.EMAIL,
    pass: process.env.PASSWORD_EMAIL,
  },
};

export default nodemailerConfig;
