import multer from "multer";
import path from "path";

// const __dirname = path.resolve();
// console.log(path.join(__dirname, "/uploads/"));

// const storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, path.join(__dirname, "uploads/"));
//   },
//   filename: function (req, file, cb) {
//     const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
//     cb(null, file.fieldname + "-" + uniqueSuffix);
//   },
// });

const fileFilter = (req, file, cb) => {
  if (!file.mimetype.match(/png|jpeg|jpg$/)) {
    cb(
      new Error("File not supported, Please upload a PNG or JPEG file"),
      false
    );
  }
  cb(null, true);
};

const upload = multer({
  storage: multer.diskStorage({}),
  fileFilter,
  limits: { fieldSize: 1024 * 1024 },
});

export default upload;
