import nodemailer from "nodemailer";
import nodemailerConfig from "./nodemailerConfig.js";
import dotenv from "dotenv";
dotenv.config();

const sendMail = async ({ email, otp }) => {
  const stmpTransporter = nodemailer.createTransport(nodemailerConfig);

  return stmpTransporter.sendMail({
    from: process.env.EMAIL, // sender address
    to: email,
    subject: "Verify Email",
    html: `<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
    <div style="margin:50px auto;width:70%;padding:20px 0">
      <div style="border-bottom:1px solid #eee">
        <a href="" style="font-size:1.4em;color: #fb246a;text-decoration:none;font-weight:600">Y4 Career</a>
      </div>
      <p style="font-size:1.1em">Hi,${email}</p>
      <p>Thank you for choosing Y4 Career. Use the following OTP to complete your reset password. OTP is valid for 15 minutes</p>
      <h2 style="background: #fb246a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;">${otp}</h2>
      <p style="font-size:0.9em;">Regards,<br />Y4 Career</p>
      <hr style="border:none;border-top:1px solid #eee" />
      <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
        <p>Y4 Career Inc</p>
        <p>490 Dev Street, Ward 3, Go Vap District</p>
        <p>Ho Chi Minh City, Viet Nam</p>
      </div>
    </div>
  </div>`,
  });
};

// const sendMail = async ({ email, otp }) => {
//   sendGrid.setApiKey(process.env.SENDGRID_API_KEY);
//   const msg = {
//     to: email, // Change to your recipient
//     from: "y4career.pbq@gmail.com", // Change to your verified sender
//     subject: "Verify OTP",
//     html: `<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
//     <div style="margin:50px auto;width:70%;padding:20px 0">
//       <div style="border-bottom:1px solid #eee">
//         <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">Y4 Career</a>
//       </div>
//       <p style="font-size:1.1em">Hi,</p>
//       <p>Thank you for choosing Y4 Career. Use the following OTP to complete your Sign Up procedures. OTP is valid for 15 minutes</p>
//       <h2 style="background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;">${otp}</h2>
//       <p style="font-size:0.9em;">Regards,<br />Y4 Career</p>
//       <hr style="border:none;border-top:1px solid #eee" />
//       <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
//         <p>Y4 Career Inc</p>
//         <p>490 Dev Street, Ward 3, Go Vap District</p>
//         <p>Ho Chi Minh City, Viet Nam</p>
//       </div>
//     </div>
//   </div>`,
//   };
//   await sendGrid.send(msg);
// };

export default sendMail;
