import bcrypt from "bcrypt";
const hashValue = async (value) => {
  const salt = await bcrypt.genSalt(10);
  const hashedValue = await bcrypt.hash(value, salt);
  return hashedValue;
};

export default hashValue;
