import jsonJobs from "./jobs.json" assert { type: "json" };
import connectDB from "./database/connect.js";
import Job from "./models/Job.js";
import dotenv from "dotenv";

dotenv.config();

const start = async () => {
  try {
    await connectDB(process.env.MONGO_URI);
    await Job.deleteMany();
    await Job.create(jsonJobs);
    console.log("Job created successfully");
    process.exit(0);
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

start();
