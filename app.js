import express from "express";
import dotenv from "dotenv";
import path from "path";
import cookieParser from "cookie-parser";
import "express-async-errors";
import cors from "cors";
import helmet from "helmet";

//Database
import connectDB from "./database/connect.js";

//Router
import jobRouter from "./routes/jobRoute.js";
import authRouter from "./routes/authRoute.js";
import blogRouter from "./routes/blogRoute.js";
import publicRouter from "./routes/publicRoute.js";
import userRouter from "./routes/userRoute.js";
import profileRouter from "./routes/profileRoute.js";
//error
import notFound from "./middlewares/not-found.js";
import errorHandler from "./middlewares/error-handler.js";

//middleware
import verifyJWT from "./middlewares/verifyJWT.js";

dotenv.config();

const app = express();
const port = 5000;

//* be able to access the data same domain
app.use(cors());
app.use(helmet());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// console.log(typeof moment().unix());

app.use("/api/v1/auth", authRouter);

app.use("/api/v1/public", publicRouter);

app.use(verifyJWT);

app.use("/api/v1/user", userRouter);

app.use("/api/v1/recruiter", jobRouter);

app.use("/api/v1/blog", blogRouter);

app.use("/api/v1/profile", profileRouter);

app.use(notFound);

app.use(errorHandler);

//* đặt ở đây có nghĩa là khi không ứng với router nào thì sẽ chạy vô middleware này, áp dụng cho tất cả các router.

app.use(notFound);

const startApp = async () => {
  try {
    await connectDB(process.env.MONGO_URI);
    app.listen(port, () => {
      console.log(`listening on port ${port}`);
    });
  } catch (err) {
    console.log(err);
  }
};

startApp();
