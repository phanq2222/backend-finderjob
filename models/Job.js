import mongoose from "mongoose";
const { Schema } = mongoose;

const JobSchema = new Schema(
  {
    jobTitle: {
      type: String,
      required: true,
    },
    requirements: {
      type: [String],
      required: true,
    },
    jobDescriptions: {
      type: [String],
      required: true,
    },
    experiences: {
      type: [String],
      required: true,
    },
    phoneNumber: {
      type: String,
      match: /(84|0[3|5|7|8|9])+([0-9]{8})\b/g,
    },
    startingSalary: Number,
    createdBy: {
      //* asign to an specific Recruiter
      type: Schema.Types.ObjectId,
      ref: "Recruiter",
    },
    image: {
      public_id: String,
      url: String,
    },

    companyName: String,
    jobNature: {
      type: String,
      enum: ["Full-time", "Part-time", "Remote"],
      default: "Full-time",
    },
  },
  { timestamps: true }
);
const WishListSchema = new Schema({
  jobSaved: [JobSchema],
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
});

const apliedJobSchema = new Schema({
  aplied: [JobSchema],
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
});

export const WishList = mongoose.model("WishList", WishListSchema);

export const Job = mongoose.model("Job", JobSchema);

export const apliedJob = mongoose.model("ApliedJob", apliedJobSchema);
