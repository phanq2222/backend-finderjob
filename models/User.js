import mongoose, { SchemaTypes } from "mongoose";
const { Schema } = mongoose;

const UserSchema = new Schema(
  {
    name: {
      type: String,
      minLength: 1,
      maxLength: 50,
    },
    email: {
      type: String,
      required: true,
      match:
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    role: {
      type: String,
      enum: ["Admin", "User", "Recruiter"],
      default: "User",
    },
    avatar: {
      public_id: { type: String, default: "ralsrp5pxtwfu9jl6o6e" },
      url: {
        type: String,
        default:
          "https://www.shareicon.net/data/2016/05/24/770137_man_512x512.png",
      },
    },
  },
  { timestamps: true }
);

const RecruiterSchema = new Schema(
  {
    ...UserSchema.obj,
    address: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    phoneNumber: {
      type: String,
      required: true,
    },
    staffingLevel: String,
    avatar: {
      public_id: { type: String, default: "dhmbb4uypa0ip3rgoldt" },
      url: {
        type: String,
        default:
          "https://static.vecteezy.com/ti/gratis-vektor/p1/2002403-mann-mit-bart-avatar-charakter-isoliert-symbol-kostenlos-vektor.jpg",
      },
    },
  },
  { timestamps: true }
);

export const User = mongoose.model("User", UserSchema);
export const Recruiter = mongoose.model("Recruiter", RecruiterSchema);
