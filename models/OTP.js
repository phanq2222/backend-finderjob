import mongoose, { SchemaTypes } from "mongoose";

const { Schema } = mongoose;

const OTPSchema = new Schema({
  expiresAt: Date,
  hashedOTP: String,
  createdAt: Date,
  email: { type: String, unique: true },
});

export default mongoose.model("OTP", OTPSchema);
