import mongoose from "mongoose";

const { Schema } = mongoose;

const TokenSchema = new Schema(
  {
    refreshToken: String,
    userAgent: String,
    belongToUser: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
  },

  { timestamps: true }
);

export default mongoose.model("Token", TokenSchema);
