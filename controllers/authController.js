import bcrypt from "bcrypt";
import { StatusCodes } from "http-status-codes";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";

import { ForbiddenError } from "../errors/forbidden.js";
import { UnauthenticatedError } from "../errors/unauthenticated.js";
import { BadRequestError } from "../errors/bad-request.js";

import { User, Recruiter } from "../models/User.js";
import OTP from "../models/OTP.js";
import Token from "../models/Token.js";

import hashValue from "../utils/hashValue.js";
import sendMail from "../utils/sendMail.js";
import { NotFoundError } from "../errors/not-found.js";

dotenv.config();

export const Register = async (req, res) => {
  const { email, password, name } = req.body;
  const isFirstAccount = (await User.countDocuments({})) === 0;
  const role = isFirstAccount ? "Admin" : "User";
  const hashedPassword = await hashValue(password);
  await User.create({ email, name, password: hashedPassword, role });
  res
    .status(StatusCodes.CREATED)
    .json({ msg: "Create an account successfully!" });
};

export const recruiterRegister = async (req, res) => {
  const {
    companyEmail,
    password,
    companyName,
    address,
    phoneNumber,
    description,
    staffingLevel,
  } = req.body;

  console.log(req.body);

  if (!req.body) throw new BadRequestError("Invalid Credentials");

  const hashedPassword = await hashValue(password);
  await Recruiter.create({
    email: companyEmail,
    name: companyName,
    password: hashedPassword,
    role: "Recruiter",
    address,
    phoneNumber,
    staffingLevel,
    description,
  });
  res
    .status(StatusCodes.CREATED)
    .json({ msg: "Create an Recruiter account successfully!" });
};

export const Login = async (req, res, next) => {
  const { email, password } = req.body;

  const existingUser = email.includes("Company")
    ? await Recruiter.findOne({ email })
    : await User.findOne({ email });

  if (!existingUser)
    throw new UnauthenticatedError(
      "Not Found User, Please Check Your Email,Password"
    );
  const match = await bcrypt.compare(
    password,
    existingUser.toObject().password
  );
  if (!match) throw new UnauthenticatedError("Invalid Credentials");
  //*HTTP-only cookies are cookies that are set with the HTTP-only flag. This flag is used to prevent client-side scripts (such as JavaScript) from accessing the cookie. By setting the HTTP-only flag, the cookie can only be accessed over the HTTP or HTTPS protocols, and not through any client-side script.
  const accessToken = jwt.sign(
    {
      userId: existingUser._id,
      name: existingUser.name,
      email: existingUser.email,
      role: existingUser.role,
      avatar: existingUser.avatar,
    },
    process.env.ACCESS_TOKEN_SECRET,
    { expiresIn: "5m" }
  );
  const refreshToken = jwt.sign(
    {
      userId: existingUser._id,
      name: existingUser.name,
      email: existingUser.email,
      role: existingUser.role,
      avatar: existingUser.avatar,
    },
    process.env.REFRESH_TOKEN_SECRET,
    { expiresIn: "1d" }
  );
  const userAgent = req.headers["user-agent"];
  await Token.create({
    userAgent,
    belongToUser: existingUser._id,
    refreshToken,
  });
  res.cookie("refreshToken", refreshToken, {
    httpOnly: true,
    // buộc phải sử dụng https
    // secure: true,
  });
  res.status(StatusCodes.OK).json({ accessToken, role: existingUser.role });
};

export const RefreshToken = async (req, res, next) => {
  const cookies = req.cookies;
  if (!cookies?.refreshToken) {
    throw new UnauthenticatedError("Unauthenticated");
  }
  const refreshToken = cookies.refreshToken;
  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, decoded) => {
    console.log(decoded);
    if (err) {
      //* refresh token invalid (expired , error)
      throw new ForbiddenError("Invalid Token");
    }
    //* when refresh token was still valid
    const accessToken = jwt.sign(
      {
        userId: decoded.userId,
        name: decoded.name,
        email: decoded.email,
        role: decoded.role,
        avatar: decoded.avatar,
      },
      process.env.ACCESS_TOKEN_SECRET,
      {
        expiresIn: "5m",
      }
    );
    res.status(StatusCodes.OK).json(accessToken);
  });
};

export const Logout = async (req, res) => {
  const cookies = req.cookies;
  const refreshToken = cookies.refreshToken;
  const existingToken = await Token.findOne({ refreshToken });
  if (!existingToken) {
    res.clearCookie("refreshToken", {
      httpOnly: true,
      secure: true,
    });
    res.clearCookie("accessToken", {
      httpOnly: true,
      secure: true,
    });
    return res.sendStatus(StatusCodes.NO_CONTENT);
  }
  existingToken.refreshToken = "";
  await existingToken.save();
  res.clearCookie("refreshToken", {
    httpOnly: true,
  });
  res.clearCookie("accessToken", {
    httpOnly: true,
  });
  res.status(StatusCodes.NO_CONTENT).json({ msg: "User Logout" });
};

export const forgotPassword = async (req, res, next) => {
  const { email } = req.body;
  try {
    const otp = Math.floor(1000 + Math.random() * 9000);
    const resultFound = email.includes("Company")
      ? await Recruiter.find({ email })
      : await User.find({ email });
    if (!resultFound) throw new NotFoundError("Not Found");
    sendMail({ email, otp });
    const hashedOTP = await hashValue(otp.toString());
    await OTP.create({
      expiresAt: Date.now() + 15 * 60 * 1000,
      createdAt: Date.now(),
      otp: hashedOTP,
      email,
    });
    res.status(StatusCodes.OK).json({
      email,
    });
  } catch (err) {
    res.json({ msg: err.message });
  }
};

export const verifyOTP = async (req, res, next) => {
  const { email, otp, newPassword } = req.body;
  const otpFound = await OTP.find({ email });
  const match = await bcrypt.compare(otp, otpFound.hashedOTP);
  if (match) {
    if (result.expiresAt < Date.now()) {
      throw new Error("Invalid OTP");
    }
    const resultFound = email.includes("Company")
      ? await Recruiter.find({ email })
      : await User.find({ email });

    const hashedPassword = hashValue(newPassword);
    resultFound.password = hashedPassword;
    resultFound.save();
    await OTP.deleteOne({ email });
    res.json({ msg: "Reset Password Sucessfull" });
  }
  await OTP.deleteOne({ email });
  res.json({ msg: "Reset Password Failed" });
};
