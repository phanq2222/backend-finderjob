import { StatusCodes } from "http-status-codes";
import _ from "lodash";

import { NotFoundError } from "../errors/not-found.js";

import { Job } from "../models/Job.js";
import { Recruiter } from "../models/User.js";

export const getAllJobs = async (req, res, next) => {
  const { search, salary, jobNature, sort } = req.query;
  const queryObject = {};
  if (search) {
    queryObject.jobTitle = { $regex: search, $options: "i" };
  }
  if (jobNature && jobNature !== "all") {
    queryObject.jobNature = jobNature;
  }
  const page = Number(req.query.page) || 1;
  const limit = Number(req.query.limit) || 10;
  const skip = (page - 1) * limit;

  let allJobsFound = await Job.find(queryObject).skip(skip).limit(limit);

  const totalJobs = await Job.countDocuments(queryObject);
  const numOfPages = Math.ceil(totalJobs / limit);

  // console.log(totalJobs);x

  if (_.isEmpty(allJobsFound)) throw new NotFoundError("Not Found");
  if (sort === "Latest") {
    allJobsFound = _.orderBy(allJobsFound, ["createdAt"], ["desc"]);
  }
  if (sort === "Oldest") {
    allJobsFound = _.orderBy(allJobsFound, ["createdAt"], ["asc"]);
  }
  if (sort === "A-Z") {
    allJobsFound = _.orderBy(allJobsFound, ["jobTitle"], ["asc"]);
  }
  if (sort === "Z-A") {
    allJobsFound = _.orderBy(allJobsFound, ["jobTitle"], ["desc"]);
  }

  res.status(StatusCodes.OK).json({
    msg: "Get All Jobs Created Successfully",
    jobs: allJobsFound,
    totalJobs,
    numOfPages,
  });
  req.query = {};
};
export const getJob = async (req, res, next) => {
  const { jobId } = req.params;
  const JobFound = await Job.findById(jobId).populate({
    path: "createdBy",
    select: "name email address staffingLevel description ",
  });
  if (!JobFound) throw new NotFoundError("Not Found Job");
  res.status(StatusCodes.OK).json(JobFound);
};
