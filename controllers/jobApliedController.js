import { StatusCodes } from "http-status-codes";
import { NotFoundError } from "../errors/not-found.js";
import { Job, apliedJob } from "../models/Job.js";
export const getApliedList = async (req, res) => {
  const apliedList = await apliedJob
    .findOne({ user: req.user.userId })
    .populate({
      path: "user",
      select: "name email",
    });
  if (!apliedList) throw new NotFoundError("Not Found");
  res.status(StatusCodes.OK).json({
    msg: "Get List Aplied Job Of User Successfully",
    ApliedList,
    total: apliedList.jobSaved.length,
  });
};

export const addJobToApliedList = async (req, res) => {
  const {
    body: { jobId },
    user: { userId },
  } = req;
  const jobFound = await Job.findById(jobId);
  if (!jobFound) throw new NotFoundError("Not Found");
  const isExisting = await apliedJob.findOne({ user: userId });
  if (!isExisting) {
    await apliedJob.create({
      user: userId,
      aplied: [],
    });
    const result = await apliedJob.findOne({ user: userId });
    result.aplied = [...result.aplied, jobFound];
    result.save();
    // console.log(result);
    return res
      .status(StatusCodes.OK)
      .json({ msg: "Job Add To ApliedList Successfully" });
  }
  isExisting.aplied = [...isExisting.aplied, jobFound];
  isExisting.save();

  res
    .status(StatusCodes.OK)
    .json({ msg: "Job Add To ApliedList Successfully" });
};

export const deleteJobFromApliedList = async (req, res) => {
  const {
    body: { jobId },
    user: { userId },
  } = req;
  if (!jobFound) throw new NotFoundError("Not Found");
  const isExisting = await apliedJob.findOne({ user: userId });
  isExisting.aplied = isExisting.aplied.filter((job) => job._id !== jobId);
  isExisting.save();

  res
    .status(StatusCodes.OK)
    .json({ msg: "Job Remove From ApliedList Successfully" });
};
