import { StatusCodes } from "http-status-codes";
import { BadRequestError } from "../errors/bad-request.js";
import { NotFoundError } from "../errors/not-found.js";
import { User, Recruiter } from "../models/User.js";
import cloudinary from "../utils/cloudinaryConfig.js";
import hashValue from "../utils/hashValue.js";
import bcrypt from "bcrypt";

export const updateProfileRecruiter = async (req, res, next) => {
  let data = { ...req.body };
  const profileFound = await Recruiter.findById(req.user.userId);
  console.log(profileFound);
  if (req.file?.path) {
    await cloudinary.uploader.destroy(profileFound.avatar.public_id);
    const result = await cloudinary.uploader.upload(req.file.path, {
      use_filename: true,
      folder: `profile_image_uploads/${
        profileFound.name - profileFound.createdBy
      }`,
    });
    profileFound.name = data.name;
    profileFound.description = data.description;
    profileFound.phoneNumber = data.phoneNumber;
    profileFound.address = data.address;
    profileFound.avatar = {
      ...profileFound.avatar,
      public_id: result.public_id,
      url: result.secure_url,
    };
    await profileFound.save();
    return res
      .status(StatusCodes.OK)
      .json({ msg: "Update Profile successfully!", profileFound });
  }
  delete data["avatar"];
  profileFound.name = data.name;
  profileFound.description = data.description;
  profileFound.phoneNumber = data.phoneNumber;
  profileFound.address = data.address;
  await profileFound.save();
  return res
    .status(StatusCodes.OK)
    .json({ msg: "Update Profile successfully!", profileFound });
};

export const changePassword = async (req, res, next) => {
  const { newPassword, currentPassword, role } = req.body;
  const profileFound =
    role === "Recruiter"
      ? await Recruiter.findById(req.user.userId)
      : await User.findById(req.user.userId);
  const match = await bcrypt.compare(currentPassword, profileFound.password);
  console.log(match);
  if (!match) {
    throw new BadRequestError("Your password is incorrect");
  }
  const hashedPassword = await hashValue(newPassword);
  profileFound.password = hashedPassword;
  profileFound.save();
  res.status(StatusCodes.OK).json({ msg: "Change Password Successfully!" });
};
