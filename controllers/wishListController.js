import { StatusCodes } from "http-status-codes";
import { NotFoundError } from "../errors/not-found.js";
import { Job, WishList } from "../models/Job.js";
export const getWishList = async (req, res) => {
  const wishList = await WishList.findOne({ user: req.user.userId }).populate({
    path: "user",
    select: "name email",
  });
  if (!wishList) throw new NotFoundError("Not Found");
  res.status(StatusCodes.OK).json({
    msg: "Get Wish List Of User Successfully",
    wishList,
    total: wishList.jobSaved.length,
  });
};

export const addJobToWishList = async (req, res) => {
  const {
    body: { jobId },
    user: { userId },
  } = req;
  console.log(userId);
  const jobFound = await Job.findById(jobId);
  if (!jobFound) throw new NotFoundError("Not Found");
  const isExisting = await WishList.findOne({ user: userId });
  if (!isExisting) {
    await WishList.create({
      user: userId,
      jobSaved: [],
    });
    const result = await WishList.findOne({ user: userId });
    result.jobSaved = [...result.jobSaved, jobFound];
    result.save();
    // console.log(result);
    return res
      .status(StatusCodes.OK)
      .json({ msg: "Job Add To Wishlist Successfully" });
  }
  isExisting.jobSaved = [...isExisting.jobSaved, jobFound];
  isExisting.save();

  res.status(StatusCodes.OK).json({ msg: "Job Add To Wishlist Successfully" });
};

export const deleteJobFromWishList = async (req, res) => {
  const {
    body: { jobId },
    user: { userId },
  } = req;
  if (!jobFound) throw new NotFoundError("Not Found");
  const isExisting = await WishList.findOne({ user: userId });
  isExisting.jobSaved = isExisting.jobSaved.filter((job) => job._id !== jobId);
  isExisting.save();

  res
    .status(StatusCodes.OK)
    .json({ msg: "Job Remove From Wishlist Successfully" });
};
