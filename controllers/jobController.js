import { StatusCodes } from "http-status-codes";
import _ from "lodash";

import cloudinary from "../utils/cloudinaryConfig.js";

import { BadRequestError } from "../errors/bad-request.js";
import { NotFoundError } from "../errors/not-found.js";

import { Job } from "../models/Job.js";
import { Recruiter } from "../models/User.js";

export const getAllJobsByRecruiter = async (req, res) => {
  const { search, salary, jobNature, sort } = req.query;
  const queryObject = {
    createdBy: req.user.userId,
  };
  if (search) {
    queryObject.jobTitle = { $regex: search, $options: "i" };
  }
  if (jobNature && jobNature !== "all") {
    queryObject.jobNature = jobNature;
  }
  const page = Number(req.query.page) || 1;
  const limit = Number(req.query.limit) || 10;
  const skip = (page - 1) * limit;

  let allJobsFound = await Job.find(queryObject).skip(skip).limit(limit);
  if (_.isEmpty(allJobsFound)) throw new NotFoundError("Not Found");
  if (sort === "lastest") {
    allJobsFound = _.orderBy(allJobsFound, ["createdAt"], ["desc"]);
  }
  if (sort === "oldest") {
    allJobsFound = _.orderBy(allJobsFound, ["createdAt"], ["asc"]);
  }
  if (sort === "a-z") {
    allJobsFound = _.orderBy(allJobsFound, ["jobTitle"], ["asc"]);
  }
  if (sort === "z-a") {
    allJobsFound = _.orderBy(allJobsFound, ["jobTitle"], ["desc"]);
  }

  res.status(StatusCodes.OK).json({
    msg: "Get All Jobs Created Successfully",
    data: allJobsFound,
    totalJob: allJobsFound.length,
  });
  req.query = {};
};

export const createJobByRecruiter = async (req, res) => {
  req.body.createdBy = req.user.userId;
  const recruiter = await Recruiter.findById(req.user.userId);
  if (!recruiter) throw new NotFoundError("Not Found Recruiter");
  const jobCreated = await Job.create({
    ...req.body,
    image: { public_id: recruiter.avatar.public_id, url: recruiter.avatar.url },
    companyName: recruiter.name,
  });
  if (!jobCreated)
    throw new BadRequestError("Invalid Data, Can't create a new job");
  res
    .status(StatusCodes.CREATED)
    .json({ message: "A Job Created Successfully", jobCreated });
};

export const getSingleJobByRecruiter = async (req, res) => {
  const {
    user: { userId },
    params: { jobId: jobId },
  } = req;
  const jobFound = await Job.find({ createdBy: userId, _id: jobId });
  if (_.isEmpty(jobFound)) throw new NotFoundError("Not Found");
  res
    .status(StatusCodes.OK)
    .json({ message: "Get Job Successfully", data: { jobFound } });
};

export const updateSingleJobByRecruiter = async (req, res) => {
  const {
    body,
    user: { userId },
    params: { jobId },
  } = req;
  const data = { ...body };
  //*compare each value in object with specific value
  if (_.values(body).every((value) => value === ""))
    throw new BadRequestError("Data Fields Can't Empty");

  const currentJob = await Job.findOne({ _id: jobId, createdBy: userId });
  if (!currentJob) throw new NotFoundError(`Not Found Job with ID ${jobId} `);

  const jobUpdated = await Job.findOneAndUpdate(
    {
      _id: jobId,
      createdBy: userId,
    },
    data,
    { new: true, runValidators: true }
  );
  res.status(StatusCodes.OK).json({ msg: jobUpdated });
};

export const deleteSingleJobByRecruiter = async (req, res) => {
  const {
    user: { userId },
    params: { jobId },
  } = req;

  const jobDeleted = await Job.findOneAndDelete({
    _id: jobId,
    createdBy: userId,
  });

  const imageId = jobDeleted.image.public_id;
  await cloudinary.uploader.destroy(imageId);

  if (!jobDeleted) throw new NotFoundError(`Can not delete with ${jobId}`);
  res
    .status(StatusCodes.OK)
    .json({ msg: "Delete a Job Sucessfully!", data: { jobDeleted } });
};
