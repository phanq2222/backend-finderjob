import _ from "lodash";
import { StatusCodes } from "http-status-codes";

import { NotFoundError } from "../errors/not-found.js";

import cloudinary from "../utils/cloudinaryConfig.js";

import Blog from "../models/Blog.js";

export const getAllBlogsByAdmin = async (req, res, next) => {
  const allBlogs = await Blog.find({});
  if (_.isEmpty(allBlogs)) {
    throw new NotFoundError("Not Found");
  }
  res
    .status(StatusCodes.OK)
    .json({ msg: "Get All Blogs SuccessFully", data: { allBlogs } });
};

export const getBlogByAdmin = async (req, res) => {
  const { blogId } = req.params;

  const blogFound = await Blog.findById(blogId);
  if (!blogFound) {
    throw new NotFoundError(`Not Found with Blog: ${blogFound._id}`);
  }
  res
    .status(StatusCodes.OK)
    .json({ msg: "Get Blog SuccessFully", data: { blogFound } });
};

export const createNewBlogByAdmin = async (req, res, next) => {
  const result = await cloudinary.uploader.upload(req.file.path, {
    use_filename: true,
    folder: `blog_image_uploads/${req.body.title}`,
  });
  const blogCreated = await Blog.create({
    ...req.body,
    image: { public_id: result.public_id, url: result.secure_url },
  });
  if (!blogCreated) {
    throw new BadRequestError("Invalid Data, Can't create new blog");
  }
  res.status(StatusCodes.CREATED).json({ msg: "New Blog Created" });
};

export const updateBlogByAdmin = async (req, res) => {
  const {
    params: { blogId },
    body: { title, content },
  } = req;
  const BlogFound = await Blog.findById(blogId);
  if (!req.file?.path) {
    BlogFound.title = title;
    BlogFound.content = content;
    BlogFound.createdBy = req.user.userId;
    BlogFound.save();
    return res
      .status(StatusCodes.OK)
      .json({ msg: "Update Blog successfully!" });
  }

  await cloudinary.uploader.destroy(BlogFound.image.public_id);
  const result = await cloudinary.uploader.upload(req.file.path, {
    use_filename: true,
    folder: `blog_image_uploads/${BlogFound.title - BlogFound.createdBy}`,
  });
  BlogFound.title = title;
  BlogFound.content = content;
  BlogFound.createdBy = req.user.userId;
  BlogFound.image = {
    ...BlogFound.image,
    public_id: result.public_id,
    url: result.secure_url,
  };
  res.status(StatusCodes.OK).json({ msg: "Update Blog successfully!" });
};
export const deleteBlogByAdmin = async (req, res) => {
  const {
    user: { userId },
    params: { blogId },
  } = req;

  const blogDeleted = await Blog.findOneAndDelete({
    _id: blogId,
    createdBy: userId,
  });
  await cloudinary.uploader.destroy(blogDeleted.image.public_id);
  if (!blogDeleted) throw new NotFoundError(`Can not delete with ${blogId}`);
  res
    .status(StatusCodes.OK)
    .json({ msg: "Delete a Job Sucessfully!", data: { blogDeleted } });
};
