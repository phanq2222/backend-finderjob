import { StatusCodes } from "http-status-codes";

const errorHandlerMiddleware = (err, req, res, next) => {
  console.log(err);
  let customError = {
    statusCode: err.statusCode || StatusCodes.INTERNAL_SERVER_ERROR,
    message: err.message || "Something wrong try later again!",
  };
  if (err.code === 11000) {
    customError.statusCode = StatusCodes.BAD_REQUEST;
    customError.message = `Email ${err.keyValue.email} already exists, Please choose another email address`;
  }
  if (err.name === "ValidatorError") {
    customError.message = Object.values(err.errors).map((err) =>
      err.message.join(", ")
    );
    customError.statusCode = StatusCodes.NOT_FOUND;
  }
  if (err.name === "CastError") {
    customError.message = `Can't Found Item ${err.name}`;
    customError.statusCode = StatusCodes.NOT_FOUND;
  }
  res.status(customError.statusCode).json({ msg: customError.message });
};
export default errorHandlerMiddleware;
