import { StatusCodes } from "http-status-codes";

const notFound = (req, res, next) => {
  res.status(StatusCodes.NOT_FOUND).json({ msg: "Not Found This Route" });
};

export default notFound;
