import { UnauthorizedError } from "../errors/unauthorized.js";

const verifyRole = (...roles) => {
  // This's middleware function to verify role
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      throw new UnauthorizedError(
        "Unauthorized, can't access  with this role "
      );
    }
    next();
  };
};

export default verifyRole;
