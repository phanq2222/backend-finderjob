import jwt from "jsonwebtoken";
import dotenv from "dotenv";

import { UnauthorizedError } from "../errors/unauthorized.js";
import { ForbiddenError } from "../errors/forbidden.js";

dotenv.config();

const verifyJWT = (req, res, next) => {
  const authHeader = req.headers["authorization"];
  if (!authHeader) throw new UnauthorizedError("Unauthorized");
  const token = authHeader.split(" ")[1]; //* split an access token from 'Bearer ....
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
    //* when token expired or has error
    if (err) throw new ForbiddenError("Invalid Token");
    req.user = { ...decoded };
    next();
  });
};

export default verifyJWT;
