import express from "express";
import {
  updateProfileRecruiter,
  changePassword,
} from "../controllers/profileController.js";

//config
import upload from "../utils/multerConfig.js";
import verifyRole from "../middlewares/verifyRole.js";

const router = express.Router();

router
  .route("/update/recruiter")
  .patch(
    verifyRole("Recruiter"),
    upload.single("avatar"),
    updateProfileRecruiter
  );
router.route("/change-password").post(changePassword);

export default router;
