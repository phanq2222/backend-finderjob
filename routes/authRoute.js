import {
  Register,
  Login,
  RefreshToken,
  Logout,
  forgotPassword,
  recruiterRegister,
  verifyOTP,
} from "../controllers/authController.js";

import express from "express";

const router = express.Router();

router.post("/user/login", Login);

// router.post("/dashboard/login", LoginDashboard);

router.post("/register", Register);

// router.post("/recruiter-register", upload.single("image"), recruiterRegister);
router.post("/recruiter-register", recruiterRegister);

router.post("/refresh-token", RefreshToken);

router.post("/forgot-password", forgotPassword);

router.post("/verify-otp", verifyOTP);

router.get("/logout", Logout);

export default router;
