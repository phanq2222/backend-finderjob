import express from "express";
import {
  addJobToWishList,
  getWishList,
  deleteJobFromWishList,
} from "../controllers/wishListController.js";
import {
  addJobToApliedList,
  getApliedList,
  deleteJobFromApliedList,
} from "../controllers/jobApliedController.js";
const router = express.Router();

router
  .route("/wishlist")
  .get(getWishList)
  .post(addJobToWishList)
  .delete(deleteJobFromWishList);
router
  .route("/aplied")
  .get(getApliedList)
  .post(addJobToApliedList)
  .delete(deleteJobFromApliedList);

export default router;
