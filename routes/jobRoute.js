import express from "express";
//controller
import {
  getAllJobsByRecruiter,
  createJobByRecruiter,
  getSingleJobByRecruiter,
  updateSingleJobByRecruiter,
  deleteSingleJobByRecruiter,
} from "../controllers/jobController.js";
//middleware
import verifyRole from "../middlewares/verifyRole.js";

const router = express.Router();

router
  .route("/")
  .get(verifyRole("Recruiter"), getAllJobsByRecruiter)
  .post(verifyRole("Recruiter"), createJobByRecruiter);

router
  .route("/:jobId")
  .get(verifyRole("Recruiter"), getSingleJobByRecruiter)
  .patch(verifyRole("Recruiter"), updateSingleJobByRecruiter)
  .delete(verifyRole("Recruiter"), deleteSingleJobByRecruiter);

export default router;
