import express from "express";
//controller
import {
  getAllBlogsByAdmin,
  createNewBlogByAdmin,
  getBlogByAdmin,
  updateBlogByAdmin,
  deleteBlogByAdmin,
} from "../controllers/blogController.js";
//config
import upload from "../utils/multerConfig.js";
//middleware
import verifyRole from "../middlewares/verifyRole.js";

const router = express.Router();

router
  .route("/")
  .get(verifyRole("Admin"), getAllBlogsByAdmin)
  .post(verifyRole("Admin"), upload.single("image"), createNewBlogByAdmin);

router
  .route("/:blogId")
  .get(verifyRole("Admin"), getBlogByAdmin)
  .patch(verifyRole("Admin"), upload.single("image"), updateBlogByAdmin)
  .delete(verifyRole("Admin"), deleteBlogByAdmin);
export default router;
