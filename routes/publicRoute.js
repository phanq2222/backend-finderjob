import express from "express";
//controller
import { getAllJobs, getJob } from "../controllers/publicController.js";
//config

const router = express.Router();

router.route("/job").get(getAllJobs);

router.route("/job/:jobId").get(getJob);

export default router;
